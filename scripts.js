//Dados da Parede 01
const buttonWall01 = document.getElementById("set-wall-01");
const doorWall01 = document.getElementById("set-door-wall-01");
const numberDoorsWall01 = document.getElementById("number-doors-wall-01");
const numberWindowsWall01 = document.getElementById("number-windows-wall-01");

//Dados da Parede 02
const buttonWall02 = document.getElementById("set-wall-02");
const doorWall02 = document.getElementById("set-door-wall-02");
const numberDoorsWall02 = document.getElementById("number-doors-wall-02");
const numberWindowsWall02 = document.getElementById("number-windows-wall-02");

//Dados da Parede 03
const buttonWall03 = document.getElementById("set-wall-03");
const doorWall03 = document.getElementById("set-door-wall-03");
const numberDoorsWall03 = document.getElementById("number-doors-wall-03");
const numberWindowsWall03 = document.getElementById("number-windows-wall-03");

//Dados da Parede04
const buttonWall04 = document.getElementById("set-wall-04");
const doorWall04 = document.getElementById("set-door-wall-04");
const numberDoorsWall04 = document.getElementById("number-doors-wall-04");
const numberWindowsWall04 = document.getElementById("number-windows-wall-04");

//Dados da quantidade de latas de tinta
const quantity18LText = document.getElementById("18L-Can");
const quantity3600MlText = document.getElementById("36L-Can");
const quantity2500MlText = document.getElementById("25L-Can");
const quantity500MlText = document.getElementById("05L-Can");

//Botão de calcular a quantidade de latas de tinta
const btnCalculatePaint = document.getElementById("btn-calculate-paint");

// Medidas e áreas das portas e janelas
const heightDoor = 1.9;
const widthDoor = 0.8;
const areaDoor = heightDoor * widthDoor;
const areaWindow = 2 * 1.2;

//Variáveis para guardar quantidade de latas
let quantity18L = 0;
let quantity3600Ml = 0;
let quantity2500Ml = 0;
let quantity500Ml = 0;

//Total de área ser pintada acumulando as 04 paredes
let allPaintedArea = 0;

//Validação da Parede 01
setWall01 = () =>{
    const heightWall01 = document.getElementById("height-wall-01");
    const widthWall01 = document.getElementById("width-wall-01");

    validateWallArea(heightWall01.value, widthWall01.value);

    if(doorWall01.value === "yes"){
        validateHeightWall(heightWall01.value);
    };

    const paintedArea = validatePaintedArea(heightWall01.value, widthWall01.value, 
        numberDoorsWall01.value, numberWindowsWall01.value);
    allPaintedArea = allPaintedArea + paintedArea;
};

//Validação da Parede 02
setWall02 = () =>{
    const heightWall02 = document.getElementById("height-wall-02");
    const widthWall02 = document.getElementById("width-wall-02");

    validateWallArea(heightWall02.value, widthWall02.value);

    if(doorWall02.value === "yes"){
        validateHeightWall(heightWall02.value);
    };

    const paintedArea = validatePaintedArea(heightWall02.value, widthWall02.value, 
        numberDoorsWall02.value, numberWindowsWall02.value);
    allPaintedArea = allPaintedArea + paintedArea;
};

//Validação da Parede 03
setWall03 = () =>{
    const heightWall03 = document.getElementById("height-wall-03");
    const widthWall03 = document.getElementById("width-wall-03");

    validateWallArea(heightWall03.value, widthWall03.value);

    if(doorWall03.value === "yes"){
        validateHeightWall(heightWall03.value);
    };

    const paintedArea = validatePaintedArea(heightWall03.value, widthWall03.value, 
        numberDoorsWall03.value, numberWindowsWall03.value);
    allPaintedArea = allPaintedArea + paintedArea;
};

//Validação da Parede 04
setWall04 = () =>{
    const heightWall04 = document.getElementById("height-wall-04");
    const widthWall04 = document.getElementById("width-wall-04");

    validateWallArea(heightWall04.value, widthWall04.value);

    if(doorWall04.value === "yes"){
        validateHeightWall(heightWall04.value);
    };

    const paintedArea = validatePaintedArea(heightWall04.value, widthWall04.value, 
        numberDoorsWall04.value, numberWindowsWall04.value);
    allPaintedArea = allPaintedArea + paintedArea;
};

//Validação da área de cada parede entre 1m² e 15m²
validateWallArea = (height, width) =>{
    const areaWall = height * width;
    if(areaWall < 1){
        alert("A Área da Parede tem que ser maior que 1m²");
    };
    if(areaWall > 15){
        alert("A Área da Parede tem que ser menor que 15m²");
    };
};

//Validação da altura da parede com porta
validateHeightWall = (heightWall) =>{
    if(heightWall < (heightDoor + 0.3)){
        alert("Paredes com porta devem ter no mínimo 2,20 metros de altura");
    };
};

//Validação da área a ser pintada para que seja maior que 50% que portas e janelas
validatePaintedArea = (heightWall, widthWall, numberDoors, numberWindows) =>{
    const areaWall = heightWall * widthWall;
    if((areaDoor * numberDoors) + (areaWindow * numberWindows) > (areaWall/2)){
        alert("A área das portas e janelas não podem ser maior que 50% da área da parede");
    };
    return(areaWall - ((areaDoor * numberDoors) + (areaWindow * numberWindows)));
}

//Cálculo da área a ser pintada
calculatePaintCans = () =>{
    let quantityLiters = allPaintedArea / 5;
    while(quantityLiters > 0){
        if(quantityLiters >= 18 || quantityLiters >=3.6){
            quantity18L++;
            quantityLiters = quantityLiters -18;
        }else if(quantityLiters >= 2.5 && quantityLiters <=3.6){
            quantity3600Ml ++;
            quantityLiters = quantityLiters - 3.6;
        }else if(quantityLiters > 1 && quantityLiters <=2.5){
            quantity2500Ml ++;
            quantityLiters = quantityLiters - 2.5;
        }else if(quantityLiters >= 0.5 && quantityLiters <=1){
            quantity500Ml ++;
            quantityLiters = quantityLiters - 0.5;
        }else{
            quantityLiters = 0;
        };
    };
    quantity18LText.innerHTML = quantity18L;
    quantity3600MlText.innerHTML = quantity3600Ml;
    quantity2500MlText.innerHTML = quantity2500Ml;
    quantity500MlText.innerHTML = quantity500Ml;
}

//Botões para setar as medidas das paredes
buttonWall01.addEventListener("click", setWall01);
buttonWall02.addEventListener("click", setWall02);
buttonWall03.addEventListener("click", setWall03);
buttonWall04.addEventListener("click", setWall04);

//Botão para calcular quantidade de latas de tinta
btnCalculatePaint.addEventListener("click", calculatePaintCans);