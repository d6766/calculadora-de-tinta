# Calculadora de Latas de Tinta

Este projeto consiste numa calculadora de latas de tinta. O usuário informa as medidas de cada parede
e se possuem portas e/ou janelas a partir dessas informações é exibida quantas latas de tinta será
necessário para pintar o espaço.

### Linguagens e tecnlogias

O projeto utilizou as seguintes linguagens:

- [x] HTML
- [x] CSS e Flexbox
- [x] JavaScript

## Como Usar

Para clonar e rodar esta aplicação você ira precisar ter o Git instalado em seu computador. Siga a linha de comando abaixo:

```
# Clone o repositório
$ git clone https://gitlab.com/d6766/calculadora-de-tinta.git

# Entre no repositório
$ cd calculadora-de-tinta

# Para rodar a aplicação abra o arquivo index.html
```

## Contribuidores

Agradecemos às seguintes pessoas que contribuíram para este projeto:

<table>
  <tr>
    <td align="center">
      <a href="https://www.linkedin.com/in/alleson-de-moura-barbosa-193802210/">
        <img src="./assets/foto.jpg" width="150px;" alt="Foto-Alleson-Barbosa"/><br>
        <sub>
          <b>Alleson Barbosa</b>
        </sub>
      </a>
    </td>
  </tr>
</table>
